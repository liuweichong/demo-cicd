@servers(['web' => 'root@142.93.125.89'])

@setup
    $repository = 'git@gitlab.com:liuweichong/demo-cicd.git';
    $releases_dir = '/var/www/html/app/releases';
    $app_dir = '/var/www/html/app';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    create_fixed_directory
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('create_fixed_directory')
    echo 'Create app and storage&inside folders and change permissions'
    mkdir -p {{ $app_dir }}
    mkdir -p {{ $app_dir }}/storage
    mkdir -p {{ $app_dir }}/storage/framework/{session,views,cache}
    mkdir -p {{ $app_dir }}/storage/app/public
    mkdir -p {{ $app_dir }}/storage/logs
    chmod -R 777 {{ $app_dir }}/storage/framework
    chown -R www-data:www-data {{ $app_dir }}/storage/framework
@endtask

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

    echo 'change bootstrap/cache permission'
    chown -R www-data:www-data {{ $app_dir }}/current/bootstrap/cache/

    echo 'add phpmyadmin link'
    ln -nfs /usr/share/phpmyadmin {{ $app_dir }}/current/public
@endtask